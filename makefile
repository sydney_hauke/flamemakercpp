#Makefile FlameMaker

CXX=g++
CFLAGS=-Wall -W -std=c++11 -O3
PROG=FlameMaker

SRCDIR=src

.PHONY: clean, mrproper
.SUFFIXES:

all: $(PROG)

debug: CFLAGS += -DDEBUG -g3
debug: $(PROG)

$(PROG):
	$(CXX) $(SRCDIR)/*.cpp -o $(PROG) $(CFLAGS)

clean:

mrproper: clean
	rm -rf $(PROG)
