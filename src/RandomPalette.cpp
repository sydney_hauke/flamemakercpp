/*
 * RandomPalette.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#include "RandomPalette.h"
#include "InterpolatedPalette.h"
#include "Color.h"

#include <cstdlib>
#include <ctime>
#include <vector>

RandomPalette::RandomPalette() : palette({Color::BLACK}) {}

RandomPalette::RandomPalette(int numberOfColors) {
    std::vector<Color> tmpPalette(numberOfColors);
    double r, g, b;

    srand(time(NULL));

    for(int i = 0; i < numberOfColors; i++) {
        r = rand()/double(RAND_MAX);
        g = rand()/double(RAND_MAX);
        b = rand()/double(RAND_MAX);

        tmpPalette[i] = Color(r,g,b);
    }

    palette = InterpolatedPalette(tmpPalette);
}

Color RandomPalette::colorForIndex(double index) const {
    return palette.colorForIndex(index);
}
