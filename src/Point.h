/*
 * Point.h
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#ifndef POINT_H_
#define POINT_H_

class Point {
	public:
		Point(double x, double y);
		Point();
		double x() const;
		double y() const;
		double r() const;
		double theta() const;

		static const Point ORIGIN;

	private:
		 double x_,y_,r_,theta_;
};

#endif /* POINT_H_ */
