/*
 * Rectangle.h
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include "Point.h"

class Rectangle {
	public:
		Rectangle();
		Rectangle(Point const& center, double width, double height);
		double right() const;
		double left() const;
		double bottom() const;
		double top() const;
		double width() const;
		double height() const;
		Point center() const;
		double aspectRatio() const;
		bool contains(Point p) const;
		Rectangle expandToAspectRatio(double aspectRatio) const;

	private:
		Point center_;
		double width_, height_;
		double left_, right_, top_, bottom_;
		double aspectRatio_;
};

#endif /* RECTANGLE_H_ */
