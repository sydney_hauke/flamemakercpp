/*
 * InterpolatedPalette.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#include "InterpolatedPalette.h"

#include <vector>
#include <cmath>

#include "Color.h"

InterpolatedPalette::InterpolatedPalette() : palette_(1, Color::BLACK), paletteSize_(1) {}

InterpolatedPalette::InterpolatedPalette(std::vector<Color> const& palette) : palette_(palette), paletteSize_(palette.size()) {}

Color InterpolatedPalette::colorForIndex(double index) const {
    int cLeft = (int)floor(index * (paletteSize_ - 1));
    int cRight = (int)ceil(index * (paletteSize_ - 1));
    double proportion = (index * (paletteSize_ - 1)) - cLeft;

    return (palette_[cLeft]).mixWith(palette_[cRight], proportion);
}
