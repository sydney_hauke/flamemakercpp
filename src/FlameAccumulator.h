/*
 * FlameAccumulator.h
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#ifndef FLAMEACCUMULATOR_H_
#define FLAMEACCUMULATOR_H_
#include <vector>

#include "InterpolatedPalette.h"
#include "Color.h"
#include "Rectangle.h"
#include "Point.h"
#include "AffineTransformation.h"

class FlameAccumulator {
	public:
		FlameAccumulator(std::vector<std::vector<unsigned int> > const& hitCount, std::vector<std::vector<double> > const& colorIndexSum);
		int width() const;
		int height() const;
		double intensity(int x, int y) const;
		Color color(InterpolatedPalette const& palette, Color background, int x, int y) const;

		class Builder {
			public:
				Builder(Rectangle const& frame, int width, int height);
				void hit(Point const& p, double colorIndex);
				FlameAccumulator build() const;

			private:
				Rectangle frame_;
				std::vector<std::vector<unsigned int>> buildAccumulator_;
				std::vector<std::vector<double>> buildColorIndexAccumulator_;
				AffineTransformation scale_, translate_;
		};

	private:
		std::vector<std::vector<unsigned int>> accumulator_;
		std::vector<std::vector<double>> colorIndexAccumulator_;
		int width_;
		int height_;
		double logMaxPoints_;
};
#endif /* FLAMEACCUMULATOR_H_ */
