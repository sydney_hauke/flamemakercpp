/*
 * Point.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#include "Point.h"
#include <cmath>

Point::Point(double x, double y) : x_(x), y_(y) {
	r_ = sqrt(x*x + y*y);
	theta_ = (atan2(y,x));
}

Point::Point() : x_(0), y_(0), r_(0), theta_(0) {}

double Point::x() const {
	return x_;
}

double Point::y() const {
	return y_;
}

double Point::r() const {
	return r_;
}

double Point::theta() const {
	return theta_;
}

const Point Point::ORIGIN = Point(0, 0);
