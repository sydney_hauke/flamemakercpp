/*
 * Variation.cpp
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#include <cmath>
#include <vector>
#include <functional>
#include "Variation.h"
#include "Point.h"

Point Variation::linear(Point p) {
	return Point(p.x(), p.y());
}

Point Variation::sinusoidal(Point p) {
	double x = sin(p.x());
	double y = sin(p.y());

	return Point(x, y);
}

Point Variation::spherical(Point p) {
	if(p.r() == 0.0) {
		return p;
	}

	double x = p.x() / (p.r() * p.r());
	double y = p.y() / (p.r() * p.r());

	return Point(x, y);
}

Point Variation::swirl(Point p) {
	double x = (p.x() * sin(p.r() * p.r())) - (p.y() * cos(p.r() * p.r()));
	double y = (p.x() * cos(p.r() * p.r())) + (p.y() * sin(p.r() * p.r()));

	return Point(x, y);
}

Point Variation::horseshoe(Point p) {
	if(p.r() == 0.0) {
		return p;
	}

	double x = ((p.x() - p.y()) * (p.x() + p.y())) / p.r();
	double y = (2 * p.x() * p.y()) / p.r();

	return Point(x, y);
}

Point Variation::bubble(Point p) {
	double x = (4 * p.x()) / ((p.r() * p.r()) + 4);
	double y = (4 * p.y()) / ((p.r() * p.r()) + 4);

	return Point(x, y);
}

std::vector<std::function<Point(Point)>> Variation::ALL_VARIATIONS = {
    linear,
    sinusoidal,
    spherical,
    swirl,
    horseshoe,
    bubble

};
