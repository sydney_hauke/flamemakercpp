/*
 * Color.h
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#ifndef COLOR_H_
#define COLOR_H_

class Color {
    public:
        static Color BLACK;
        static Color WHITE;
        static Color RED;
        static Color GREEN;
        static Color BLUE;

        Color();
        Color(double r, double g, double b);
        double red() const;
        double green() const;
        double blue() const;
        Color mixWith(Color that, double proportion) const;
        int asPackedRGB() const;
        static int sRGBEncode(double v, int max);

    private:
        double r_,g_,b_;
};

#endif /* COLOR_H_ */
