/*
 * Rectangle.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#include "Rectangle.h"

Rectangle::Rectangle() : center_(Point::ORIGIN) {}

Rectangle::Rectangle(Point const& center, double width, double height) {
	center_ = center;
	width_ = width;
	height_ = height;

	left_ = center.x() - width / 2;
	right_ = center.x() + width / 2;
	bottom_ = center.y() - height / 2;
	top_ = center.y() + height / 2;

	aspectRatio_ = width/height;
}

double Rectangle::right() const {
	return right_;
}

double Rectangle::left() const {
	return left_;
}

double Rectangle::bottom() const {
	return bottom_;
}

double Rectangle::top() const {
	return top_;
}

double Rectangle::width() const {
	return width_;
}

double Rectangle::height() const {
	return height_;
}

Point Rectangle::center() const {
	return center_;
}

double Rectangle::aspectRatio() const {
	return aspectRatio_;
}

bool Rectangle::contains(Point p) const {
	if(p.x() >= left_ && p.x() < right_ && p.y() < top_ && p.y() >= bottom_) {
		return true;
	}
	else {
		return false;
	}
}

Rectangle Rectangle::expandToAspectRatio(double aspectRatio) const {
	if(aspectRatio > this->aspectRatio_) {
		return Rectangle(center_, height_ * aspectRatio, height_);
	}
	else if(aspectRatio < this->aspectRatio_) {
		return Rectangle(center_, width_, width_ / aspectRatio);
	}
	else {
		return Rectangle(center_, width_, height_);
	}
}
