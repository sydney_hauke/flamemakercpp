/*
 * Flame.cpp
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#include "Flame.h"
#include <ctime>
#include <cmath>

Flame::Flame(std::vector<FlameTransformation> const& transformations, Rectangle const& frame, int width, int height) : transformationList_(transformations), flame_(frame, width, height), width_(width), height_(height) {}

void Flame::compute(int density) {
	int iterations = width_ * height_;
	int listSize = transformationList_.size();

	srand(time(NULL));

	int i;
	double previousColorIndex = 0, actualColorIndex = 0, transformationColorIndex = 0;
	Point p = Point::ORIGIN;

	for(int n = 0; n < density; n++) {
		for(int m = 0; m < iterations; m++) {
			i = rand() % listSize;
			transformationColorIndex = colorIndex(i);
			actualColorIndex = 0.5 * (transformationColorIndex + previousColorIndex);

			p = (transformationList_[i]).transformPoint(p);
			flame_.hit(p, actualColorIndex);

			previousColorIndex = actualColorIndex;
		}
	}
}

FlameAccumulator Flame::getFlameAccumulator() const {
	return flame_.build();
}

double Flame::colorIndex(unsigned int index) const {
	if(index == 0 || index == 1) return index;
	else {
		double pow2log2 = pow(2.0, (int)ceil(log(index/log(2))));

		return ((2 * index) - pow2log2 - 1) / pow2log2;
	}
}

Flame::Builder::Builder() : transformationList_(0), frame_(Point::ORIGIN, 1, 1), width_(100), height_(100) {}

Flame::Builder::Builder(std::vector<FlameTransformation> const& transformations) : transformationList_(transformations) {}

int Flame::Builder::transformationCount() const {
	return transformationList_.size();
}

void Flame::Builder::addTransformation(FlameTransformation const& transformation) {
	transformationList_.push_back(transformation);
}

AffineTransformation Flame::Builder::affineTransformation(int index) const {
	return FlameTransformation::Builder(transformationList_[index]).affineTransformation();
}

void Flame::Builder::setAffineTransformation(int index, AffineTransformation newTransformation) {
	FlameTransformation::Builder newFlameTransformation(transformationList_[index]);
	newFlameTransformation.setAffineTransformation(newTransformation);
	transformationList_[index] = newFlameTransformation.build();
}

double Flame::Builder::variationWeight(int index, int indexVariation) const {
	return FlameTransformation::Builder(transformationList_[index]).variationWeight(indexVariation);
}

bool Flame::Builder::setVariationWeight(int index, int indexVariation, double newWeight) {
	FlameTransformation::Builder newFlameTransformation(transformationList_[index]);
	bool newValue = newFlameTransformation.setVariationWeight(indexVariation, newWeight);
	transformationList_[index] = newFlameTransformation.build();

	return newValue;
}

void Flame::Builder::removeTransformation(int index) {
	transformationList_.erase(transformationList_.begin()+index);
}

void Flame::Builder::setFrame(Rectangle const& frame) {
	frame_ = frame;
}

void Flame::Builder::setWidth(int width) {
	width_ = width;
}

void Flame::Builder::setHeight(int height) {
	height_ = height;
}

Flame Flame::Builder::build() const {
	return Flame(transformationList_, frame_, width_, height_);
}
