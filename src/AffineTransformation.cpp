/*
 * AffineTransformation.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#include "AffineTransformation.h"
#include "Point.h"
#include <cmath>

AffineTransformation::AffineTransformation(double a, double b, double c, double d, double e, double f) : a_(a), b_(b), c_(c), d_(d), e_(e), f_(f) {}

AffineTransformation::AffineTransformation() : a_(1), b_(1), c_(1), d_(1), e_(1), f_(1) {}

AffineTransformation AffineTransformation::newTranslation(double dx, double dy) {
	return AffineTransformation(1, 0, dx, 0, 1, dy);
}

AffineTransformation AffineTransformation::newRotation(double theta) {
	return AffineTransformation(cos(theta), -sin(theta), 0, sin(theta), cos(theta), 0);
}

AffineTransformation AffineTransformation::newScaling(double sx, double sy) {
	return AffineTransformation(sx, 0, 0, 0, sy, 0);
}

AffineTransformation AffineTransformation::newShearX(double sx) {
	return AffineTransformation(1, sx, 0, 0, 1, 0);
}

AffineTransformation AffineTransformation::newShearY(double sy) {
	return AffineTransformation(1, 0, 0, sy, 1, 0);
}

AffineTransformation AffineTransformation::composeWith(AffineTransformation that) {
	double aNew = (a_ * that.a_) + (b_ * that.d_);
	double bNew = (a_ * that.b_) + (b_ * that.e_);
	double cNew = (a_ * that.c_) + (b_ * that.f_) + c_;
	double dNew = (d_ * that.a_) + (e_ * that.d_);
	double eNew = (d_ * that.b_) + (e_ * that.e_);
	double fNew = (d_ * that.c_) + (e_ * that.f_) + f_;

	return AffineTransformation(aNew, bNew, cNew, dNew, eNew, fNew);
}

Point AffineTransformation::transformPoint(Point p) const {
#ifdef SIMD
	// TODO : implement SIMD computation of a transformation
#else
	double x = (a_ * p.x()) + (b_ * p.y()) + c_;
	double y = (d_ * p.x()) + (e_ * p.y()) + f_;
#endif

	return Point(x, y);
}

const AffineTransformation AffineTransformation::IDENTITY = AffineTransformation(1, 0, 0, 0, 1, 0);
