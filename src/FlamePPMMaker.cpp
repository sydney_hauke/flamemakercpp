#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include "Color.h"
#include "InterpolatedPalette.h"
#include "Rectangle.h"
#include "Point.h"
#include "FlameTransformation.h"
#include "FlameAccumulator.h"
#include "Flame.h"

static void generateImage(std::string fileName, FlameAccumulator fractal, InterpolatedPalette const& palette) {
        std::ofstream file(fileName);

        std::cout << "Ecriture du fichier " << fileName << std::endl;
        file << "P3 ";
        file << fractal.width() << " " << fractal.height();
        file << " 100 ";

        for(int i = fractal.height()-1; i >= 0; i--) {
            for(int j = 0; j < fractal.width(); j++) {
                Color color = fractal.color(palette, Color::BLACK, j, i);
                file << Color::sRGBEncode(color.red(), 100) << " " << Color::sRGBEncode(color.green(), 100) << " " << Color::sRGBEncode(color.blue(), 100) << " ";
            }

            file << std::endl;
        }

        file.close();
}

int main(void) {
	Rectangle frame(Point(0.1, 0.1), 3, 3);

	std::vector<Color> colorList;
	colorList.push_back(Color::RED);
	colorList.push_back(Color::GREEN);
	colorList.push_back(Color::BLUE);
	InterpolatedPalette palette(colorList);

	/* Variations to be applied to the affine transformations */
	double variation1[] = {0.5, 0, 0, 0.4, 0, 0};
	double variation2[] = {1, 0, 0.1, 0, 0, 0};
	double variation3[] = {1, 0, 0, 0, 0, 0};
	AffineTransformation transformation1(0.7124807, -0.4113509, -0.3, 0.4113513, 0.7124808, -0.7);
	AffineTransformation transformation2(0.3731079, -0.6462417, 0.4, 0.6462414, 0.3731076, 0.3);
	AffineTransformation transformation3(0.0842641, -0.314478, -0.1, 0.314478, 0.0842641, 0.3);

	/* Creating a list of transformations */
	std::vector<FlameTransformation> transformations(0);
	transformations.push_back(FlameTransformation(transformation1, variation1));
	transformations.push_back(FlameTransformation(transformation2, variation2));
	transformations.push_back(FlameTransformation(transformation3, variation3));
	Flame::Builder turbulenceGeneratorBuilder(transformations); // Builds a new fractal, ready to be computed
	turbulenceGeneratorBuilder.setFrame(frame);
	turbulenceGeneratorBuilder.setHeight(500);
	turbulenceGeneratorBuilder.setWidth(500);

	Flame turbulenceGenerator(turbulenceGeneratorBuilder.build());

	std::cout << "Calcul turbulence en cours..." << std::endl;
	turbulenceGenerator.compute(50);
	generateImage("turbulence.ppm", turbulenceGenerator.getFlameAccumulator(), palette);

	return 0;
}
