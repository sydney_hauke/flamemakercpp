/*
 * Color.cpp
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#include "Color.h"
#include <cmath>
#include <stdexcept>

using namespace std;

Color::Color() : r_(0), g_(0), b_(0) {}

Color::Color(double r, double g, double b) : r_(r), g_(g), b_(b) {}

double Color::red() const {
    return r_;
}

double Color::blue() const {
    return b_;
}

double Color::green() const {
    return g_;
}

Color Color::mixWith(Color that, double proportion) const {
    double r,g,b;

    r = that.r_ * proportion + r_ * (1 - proportion);
    g = that.g_ * proportion + g_ * (1 - proportion);
    b = that.b_ * proportion + b_ * (1 - proportion);

    return Color(r, g, b);
}

int Color::asPackedRGB() const {
    int r = sRGBEncode(r_, 255);
    int g = sRGBEncode(g_, 255);
    int b = sRGBEncode(b_, 255);

    return (r << 16) | (g << 8) | b;
}

int Color::sRGBEncode(double v, int max) {
    double encodedValue;

    if(v <= 0.0031308) {
        encodedValue = 12.92 * v;
    }
    else {
        encodedValue = 1.055 * pow(v, 1/2.4) - 0.055;
    }

    return (int)(encodedValue * max);
}

Color Color::BLACK = Color(0,0,0);
Color Color::RED = Color(1,0,0);
Color Color::GREEN = Color(0,1,0);
Color Color::BLUE = Color(0,0,1);
Color Color::WHITE = Color(1,1,1);


