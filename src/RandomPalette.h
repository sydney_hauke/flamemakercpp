/*
 * RandomPalette.h
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#ifndef RANDOMPALETTE_H_
#define RANDOMPALETTE_H_

#include "Color.h"
#include "InterpolatedPalette.h"

class RandomPalette {
    public:
		RandomPalette();
        RandomPalette(int numberOfColors);
        Color colorForIndex(double index) const;

    private:
        InterpolatedPalette palette;
};

#endif /* RANDOMPALETTE_H_ */
