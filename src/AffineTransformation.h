/*
 * AffineTransformation.h
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#ifndef AFFINETRANSFORMATION_H_
#define AFFINETRANSFORMATION_H_

#include "Point.h"

class AffineTransformation {
	public:
		AffineTransformation(double a, double b, double c, double d, double e, double f);
		AffineTransformation();
		static AffineTransformation newTranslation(double dx, double dy);
		static AffineTransformation newRotation(double theta);
		static AffineTransformation newScaling(double sx, double sy);
		static AffineTransformation newShearX(double sx);
		static AffineTransformation newShearY(double sy);
		AffineTransformation composeWith(AffineTransformation that);
		Point transformPoint(Point p) const;
		double translationX() const;
		double translationY() const;

		static const AffineTransformation IDENTITY;

	private:
		double a_, b_, c_, d_, e_, f_;
};

#endif /* AFFINETRANSFORMATION_H_ */
