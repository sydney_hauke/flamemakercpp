/*
 * FlameAccumulator.cpp
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#include "FlameAccumulator.h"

#include <cmath>
#include "AffineTransformation.h"

FlameAccumulator::FlameAccumulator(std::vector<std::vector<unsigned int>> const& hitCount, std::vector<std::vector<double>> const& colorIndexSum) : accumulator_(hitCount.size(), std::vector<unsigned int>(hitCount[0].size(), 0)), colorIndexAccumulator_(hitCount.size(), std::vector<double>(hitCount[0].size(), 0.0)) {
	width_ = hitCount[0].size();
	height_ = hitCount.size();

	unsigned int max = 0;

	/*
	 * Copy of the two arrays. Could be done by the method clone()
	 * but we profit of the two loops to determine the maximum
	 * number of points in a box of the accumulator
	 */
	for(int i = 0; i < height_; i++) {
		for(int j = 0; j < width_; j++) {
			accumulator_[i][j] = hitCount[i][j];
            this->colorIndexAccumulator_[i][j] = colorIndexSum[i][j];

            if(accumulator_[i][j] > max) {
                max = accumulator_[i][j];
            }
		}
	}

    logMaxPoints_ = log(max + 1);
}

int FlameAccumulator::width() const {
	return width_;
}

int FlameAccumulator::height() const {
	return height_;
}

double FlameAccumulator::intensity(int x, int y) const {
	return (log((double)accumulator_[y][x] + 1)/logMaxPoints_);
}

Color FlameAccumulator::color(InterpolatedPalette const& palette, Color background, int x, int y) const {
	if(colorIndexAccumulator_[y][x] != 0) {
		Color properColor = palette.colorForIndex((double)colorIndexAccumulator_[y][x]/(double)accumulator_[y][x]);
		double m_intensity = intensity(x, y);

		return background.mixWith(properColor, m_intensity);
	}
	else {
		return background;
	}
}

FlameAccumulator::Builder::Builder(Rectangle const& frame, int width, int height) : frame_(frame), buildAccumulator_(height, std::vector<unsigned int>(width, 0)), buildColorIndexAccumulator_(height, std::vector<double>(width, 0.0)) {
	double scaleToAccumulatorX = (width / (frame.right() - frame.left()));
	double scaleToAccumulatorY = (height / (frame.top() - frame.bottom()));

	scale_ = AffineTransformation::newScaling(scaleToAccumulatorX, scaleToAccumulatorY);
	translate_ = AffineTransformation::newTranslation(-frame.left(), -frame.bottom());
}

void FlameAccumulator::Builder::hit(Point const& p, double colorIndex) {
	Point displacedPoint;
	int x, y;

	if(frame_.contains(p)) {
		/* Moves and scales to the point to the dimension of the accumulator */
		displacedPoint = translate_.transformPoint(p);
		displacedPoint = scale_.transformPoint(displacedPoint);

		x = (int)floor(displacedPoint.x());
		y = (int)floor(displacedPoint.y());

		buildAccumulator_[y][x] += 1;
		buildColorIndexAccumulator_[y][x] += colorIndex;
	}
}

FlameAccumulator FlameAccumulator::Builder::build() const {
	return FlameAccumulator(buildAccumulator_, buildColorIndexAccumulator_);
}
