/*
 * FlameTransformation.h
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#ifndef FLAMETRANSFORMATION_H_
#define FLAMETRANSFORMATION_H_

#include <vector>
#include "AffineTransformation.h"

class FlameTransformation {
	public:
		FlameTransformation(AffineTransformation const& affineTransformation, double variationWeight[6]);
		FlameTransformation();
		Point transformPoint(Point p) const;

		class Builder {
			public:
				Builder(FlameTransformation const& flameTransformation);
				AffineTransformation affineTransformation() const;
				double variationWeight(int index) const;
				bool setVariationWeight(int index, double variationWeight);
				void setAffineTransformation(AffineTransformation affineTransformation);
				FlameTransformation build();

			private:
				double variationWeightBuilder_[6];
				AffineTransformation transformationBuilder_;
		};

	private:
		double variationWeight_[6];
		AffineTransformation transformation_;
};

#endif /* FLAMETRANSFORMATION_H_ */
