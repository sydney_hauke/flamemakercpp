/*
 * FlameTransformation.cpp
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#include "FlameTransformation.h"
#include "AffineTransformation.h"
#include "Variation.h"

FlameTransformation::FlameTransformation(AffineTransformation const& affineTransformation, double variationWeight[6]) : transformation_(affineTransformation) {
	for(int i = 0; i < 6; i++) {
		variationWeight_[i] = variationWeight[i];
	}
}

FlameTransformation::FlameTransformation() {}

Point FlameTransformation::transformPoint(Point p) const {
	Point affineTransformedPoint = transformation_.transformPoint(p);
	Point r;
	double finalX = 0, finalY = 0;

	for(int i = 0; i < 6; i++) {
		if(variationWeight_[i] == 0.0) continue;

		r = Variation::ALL_VARIATIONS[i](affineTransformedPoint);
		finalX += r.x() * variationWeight_[i];
		finalY += r.y() * variationWeight_[i];
	}

	return Point(finalX, finalY);
}

FlameTransformation::Builder::Builder(FlameTransformation const& flameTramsformation) : transformationBuilder_(flameTramsformation.transformation_) {}

AffineTransformation FlameTransformation::Builder::affineTransformation() const {
	return this->transformationBuilder_;
}

double FlameTransformation::Builder::variationWeight(int index) const {
	return this->variationWeightBuilder_[index];
}

bool FlameTransformation::Builder::setVariationWeight(int index, double variationWeight) {
	if(this->variationWeightBuilder_[index] != variationWeight) {
		this->variationWeightBuilder_[index] = variationWeight;
		return true;
	}

	return false;
}

void FlameTransformation::Builder::setAffineTransformation(AffineTransformation affineTransformation) {
	this->transformationBuilder_ = affineTransformation;
}

FlameTransformation FlameTransformation::Builder::build() {
	return FlameTransformation(transformationBuilder_, variationWeightBuilder_);
}
