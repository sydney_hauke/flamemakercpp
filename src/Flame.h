/*
 * Flame.h
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#ifndef FLAME_H_
#define FLAME_H_

#include <vector>
#include "FlameAccumulator.h"
#include "FlameTransformation.h"
#include "Variation.h"
#include "Rectangle.h"
#include "AffineTransformation.h"

class Flame {
	public:
		void compute(int density);
		FlameAccumulator getFlameAccumulator() const;

		class Builder {
			public:
				Builder();
				Builder(std::vector<FlameTransformation> const& transformations);
				int transformationCount() const;
				void addTransformation(FlameTransformation const& transformation);
				AffineTransformation affineTransformation(int index) const;
				void setAffineTransformation(int index, AffineTransformation newTransformation);
				double variationWeight(int index, int indexVariation) const;
				bool setVariationWeight(int index, int indexVariation, double newWeight);
				void removeTransformation(int index);
				void setFrame(Rectangle const& frame);
				void setWidth(int width);
				void setHeight(int weight);
				Flame build() const;

			private:
				void checkIndex(unsigned int index) const;

				std::vector<FlameTransformation> transformationList_;
				Rectangle frame_;
				int width_, height_;
		};

	private:
		Flame(std::vector<FlameTransformation> const& transformations, Rectangle const& frame, int width, int height);
		double colorIndex(unsigned int index) const;

		std::vector<FlameTransformation> transformationList_;
		FlameAccumulator::Builder flame_;
		int width_, height_;
};

#endif /* FLAME_H_ */
