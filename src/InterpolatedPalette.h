/*
 * InterpolatedPalette.h
 *
 *  Created on: 30 juin 2014
 *      Author: sydney
 */

#ifndef INTERPOLATEDPALETTE_H_
#define INTERPOLATEDPALETTE_H_

#include "Color.h"

#include <vector>

class InterpolatedPalette {
    public:
		InterpolatedPalette();
        InterpolatedPalette(std::vector<Color> const& palette);
        Color colorForIndex(double index) const;
    private:
        std::vector<Color> palette_;
        int paletteSize_;
};

#endif /* INTERPOLATEDPALETTE_H_ */
