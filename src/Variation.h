/*
 * Variation.h
 *
 *  Created on: 1 juil. 2014
 *      Author: sydney
 */

#ifndef VARIATION_H_
#define VARIATION_H_

#include <vector>
#include <array>
#include <functional>

#include "Point.h"

class Variation {
public:
	static std::vector<std::function<Point(Point)>> ALL_VARIATIONS;
    static std::function<Point(Point)> LINEAR;

private:
	static Point linear(Point p);
	static Point sinusoidal(Point p);
	static Point spherical(Point p);
	static Point swirl(Point p);
	static Point horseshoe(Point p);
	static Point bubble(Point p);
};

#endif /* VARIATION_H_ */
